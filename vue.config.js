module.exports = {
    publicPath: process.env.NODE_ENV === 'production'
      ? process.argv[4] === '--pf' ? process.env.VUE_APP_URL_ASSETS_PF : process.argv[4] === '--pj' ? process.env.VUE_APP_URL_ASSETS_PJ : '/'
      : '/'
  }