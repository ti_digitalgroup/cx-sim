import moment from 'moment';

function validate(dateNascimento){
    let dt = moment(dateNascimento,'DD/MM/YYYY').format('YYYY-MM-DD');
    let dataAtual = moment();
      
    if(dataAtual.diff(dt, 'years')>80){
        return false;
    } else {
        return true;
    }
}

export default validate;