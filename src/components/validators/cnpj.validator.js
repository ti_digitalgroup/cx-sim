import CnpjValidate from '../rules/cnpj.js'
const validator = {
  getMessage (field, args) {
    return 'Invalid CNPJ'
  },
  validate (value, args) {
    return CnpjValidate(value)
  }
}
export default validator