import DateOldValidate from '../rules/dateOld.js'
const validator = {
  getMessage (field, args) {
    return 'Invalid Date'
  },
  validate (value, args) {
    return DateOldValidate(value)
  }
}
export default validator