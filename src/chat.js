
export const chatMixin = {
    methods: {
      abrirAtendimento: function(cpf, protocolo, nome, token, cnpj = '', nome_empresa = '') {
        setGuestToken(token,function(){
          abrirChat(cpf, protocolo, nome, token, cnpj, nome_empresa )
        },function(){
          abrirChat(cpf, protocolo, nome, token, cnpj, nome_empresa )
        });
      },
      registrarChat: function() {
        registerChatScript(function(){
          window.RocketChat.livechat.hideWidget();
        });
      },
    }
  }
  
  const CHAT_BASE_URL = process.env.VUE_APP_URL_CHAT;
  const CHAT_BASE_PAGE = `${CHAT_BASE_URL}/livechat?mode=popout`; //mode=popout
  const CHAT_BASE_SRC = `${CHAT_BASE_URL}/livechat/rocketchat-livechat.min.js?_=201903270000`;
  const CHAT_BASE_URL_TKN = `${CHAT_BASE_URL}/jwt/api/token`;
  const department = process.env.VUE_APP_CHAT_DEPARTAMENT;
  
  
  function abrirChat(cpf, protocolo, nome, token, cnpj = '', nome_empresa = '') {
    registerVisitor(department,cpf,protocolo,nome,cnpj,nome_empresa,function(rocketchat) {
      rocketchat.hideWidget();
      window.postMessage({'src': 'rocketchat', fn: 'hideWidget', args: []} , '*');
      setTimeout(function(){
        w = window.open(CHAT_BASE_PAGE,'about:blank','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=500,height=700',true);
        rocketchat.hideWidget();
      },1250)
    });
  }

  function registerChatScript(onLoadScript) {
    if (!window.RocketChat) {
      window.RocketChat = function(c) {
        window.RocketChat._.push(c);
      };
    }
    window.RocketChat._ = [];
    window.RocketChat.url = CHAT_BASE_PAGE;

    const s = document.createElement("script");
    s.type = "text/javascript";
    s.src = CHAT_BASE_SRC;
    s.async = true;
    s.onload = onLoadScript;
    document.body.appendChild(s);
  }

  function registerVisitor(department, cpf, protocolo,
                           nome, cnpj = "", nome_empresa = "", callBack ) 
  {
    const tokenRoom = "caixasim-" + cpf;

    window.RocketChat(function() {
      const complementar =
        cnpj != ""
          ? [
              { key: "cnpjCliente", value: cnpj, overwrite: true },
              { key: "nome_empresaCliente", value: nome_empresa, overwrite: true }
            ]
          : [];
      this.registerGuest({
        name: nome,
        token: tokenRoom, //token_reg,
        department: department,
        reason: "CAIXA Sim", 
        customFields: [
          { key: "protocoloCliente", value: protocolo, overwrite: true },
          { key: "cpfCliente", value: cpf, overwrite: true },
          { key: "nomeCliente", value: nome, overwrite: true },
          ...complementar
        ]
      });
      const __this = this;
      this.onChatStarted(function() {
        let complementar = [];

        __this.setCustomField('cpf', cpf);
        __this.setCustomField('protocolo', protocolo);
        __this.setCustomField('nome', nome);

        if (cnpj != '') {
          __this.setCustomField('cnpj', cnpj);
          __this.setCustomField('nome_empresa', nome_empresa);
        }

        console.log("__this: ",__this);
      });
      callBack && callBack(this);
    });
  }

  function setGuestToken(token, sucess, fail) {
    $.post(CHAT_BASE_URL_TKN, {
      token: token,
      withCredentials: true
    })
      .always(function(data) {
        console.log(data);
        sucess && sucess();
      })
      .fail(function(data) {
        console.log(data);
        fail && fail();
      });
  }

