import Vue from 'vue'
import Router from 'vue-router'
import App from './App.vue'
import VueTheMask from 'vue-the-mask'
import VeeValidate, { Validator } from 'vee-validate';
import CpfValidator from './components/validators/cpf.validator.js'
import CnpjValidator from './components/validators/cnpj.validator.js'
import DateOldValidator from './components/validators/dateOld.validator.js'
import money from 'v-money'

import VueSlider from 'vue-slider-component'
import 'vue-slider-component/theme/default.css'

import vmodal from 'vue-js-modal'

import VueLoading from 'vuejs-loading-plugin'
import moment from 'moment'

import PassoUm from './components/pf/passo-um.vue'
import PassoDois from './components/pf/passo-dois.vue'
import PassoTres from './components/pf/passo-tres.vue'
import PassoQuatro from './components/pf/passo-quatro.vue'
import PassoCinco from './components/pf/passo-cinco.vue'
import PassoSeis from './components/pf/passo-seis.vue'

import PassoUmPJ from './components/pj/passo-um.vue'
import PassoDoisPJ from './components/pj/passo-dois.vue'
import PassoTresPJ from './components/pj/passo-tres.vue'


Validator.extend('cpf', CpfValidator);
Validator.extend('cnpj', CnpjValidator);
Validator.extend('dateOld', DateOldValidator);


const dictionary = {
  pt: {
    messages: {
      cpf: () => 'CPF inválido.',
      cnpj: () => 'CNPJ inválido.',
      required: () => 'Campo obrigatório.',
      date_format: () => "Data inválida.",
      email: () => "Formato de e-mail inválido.",
      length: () => "Formato inválido.",
      dateOld: () => "Idade maior que 80 anos."
    }
  }
}

Vue.prototype.moment = moment
Vue.config.productionTip = false
Vue.use(VueTheMask)
Vue.use(VeeValidate, {locale: 'pt', dictionary: dictionary});
Vue.use(money, {precision: 4})

Vue.component('VueSlider', VueSlider)
Vue.use(vmodal)

Vue.use(VueLoading, {
  text: 'Carregando ...'
});


function getParameterByName(name) {
  var match = RegExp('[?&]' + name + '=([^&]*)').exec(window.location.search);
  return match && decodeURIComponent(match[1].replace(/\+/g, ' '));
}

var login = getParameterByName("login");

const primeiroComponet = PassoUm;

if(login){
  
  var keycloak = Keycloak(process.env.VUE_APP_URL_JSON_SSO); 
    keycloak.init().success(function(authenticated) {
      if(login && !authenticated){
        keycloak.login();
      }

      
      if(authenticated){

        Vue.prototype.$vToken = keycloak.token;
        Vue.prototype.$keycloak = keycloak;
        
        localStorage.token = keycloak.token;

        Vue.use(Router)

        var router = new Router({
          scrollBehavior() {
            return { x: 0, y: 0 };
          },
          routes: [
            {
              path: '',
              name: 'passo-um',
              component: primeiroComponet
            },
            {
              path: '/pf/passo-dois',
              name: 'passo-dois',
              component: PassoDois
            },
            {
              path: '/pf/passo-tres',
              name: 'passo-tres',
              component: PassoTres
            },
            {
              path: '/pf/passo-quatro',
              name: 'passo-quatro',
              component: PassoQuatro
            },
            {
              path: '/pf/passo-cinco',
              name: 'passo-cinco',
              component: PassoCinco
            },
            {
              path: '/pf/passo-seis',
              name: 'passo-seis',
              component: PassoSeis
            },
            {
              path: '/pj/',
              name: 'passo-um-pj',
              component: PassoUmPJ
            },
            {
              path: '/pj/passo-dois',
              name: 'passo-dois-pj',
              component: PassoDoisPJ
            },
            {
              path: '/pj/passo-tres',
              name: 'passo-tres-pj',
              component: PassoTresPJ
            },
          ]
        });

        new Vue({
          router,
          render: h => h(App),
        }).$mount('#app')
      }
      
    });

} else {

  Vue.use(Router)

  var router = new Router({
    scrollBehavior() {
      return { x: 0, y: 0 };
    },
    routes: [
      {
        path: '',
        name: 'passo-um',
        component: primeiroComponet
      },
      {
        path: '/pf/passo-dois',
        name: 'passo-dois',
        component: PassoDois
      },
      {
        path: '/pf/passo-tres',
        name: 'passo-tres',
        component: PassoTres
      },
      {
        path: '/pf/passo-quatro',
        name: 'passo-quatro',
        component: PassoQuatro
      },
      {
        path: '/pf/passo-cinco',
        name: 'passo-cinco',
        component: PassoCinco
      },
      {
        path: '/pf/passo-seis',
        name: 'passo-seis',
        component: PassoSeis
      },
      {
        path: '/pj/',
        name: 'passo-um-pj',
        component: PassoUmPJ
      },
      {
        path: '/pj/passo-dois',
        name: 'passo-dois-pj',
        component: PassoDoisPJ
      },
      {
        path: '/pj/passo-tres',
        name: 'passo-tres-pj',
        component: PassoTresPJ
      },
    ]
  });

  new Vue({
    router,
    render: h => h(App),
  }).$mount('#app');
}
